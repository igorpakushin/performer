package com.yme.performer.forms.hoover;

import com.yme.performer.forms.MainFormViewBase;
import com.yme.performer.interpretator.hoover.InterpretatorHoover;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.ui.component.hoover.GameFieldHooverController;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

public abstract class MainFormViewHooverBuilder extends MainFormViewBase {

    static java.util.List<String> keywords;
    private ResourceBundle bundle;

    public MainFormViewHooverBuilder() {
        super();

        setGameField(new GameFieldHooverController());

        Dimension requiredSize = getGameField().getRequiredSize();
        requiredSize.width *= 1.5;  // codeField will be 50% paintPanel wide
        Dimension toolbarFieldPreferredSize = getToolbarField().getPreferredSize();
        requiredSize.height += toolbarFieldPreferredSize.height + 50;
        setSize(requiredSize);

        setTitle("Main Form Hoover");

        keywords = new ArrayList<String>() {{
            add(bundle.getString("left"));
            add(bundle.getString("right"));
            add(bundle.getString("up"));
            add(bundle.getString("down"));

            add(bundle.getString("put"));
            add(bundle.getString("take"));
            add(bundle.getString("save"));

            add(bundle.getString("can_left"));
            add(bundle.getString("can_right"));
            add(bundle.getString("can_up"));
            add(bundle.getString("can_down"));

            add(bundle.getString("is_bag_empty"));
            add(bundle.getString("is_bag_not_empty"));
            add(bundle.getString("is_cell_empty"));
            add(bundle.getString("is_cell_not_empty"));

            add(bundle.getString("procedure"));
            add(bundle.getString("if"));
            add(bundle.getString("else"));
            add(bundle.getString("while"));
            add(bundle.getString("do"));
            add(bundle.getString("end"));
        }};
    }

    @Override
    protected java.util.List<String> getKeywordsList() {
        return keywords;
    }

    @Override
    protected InterpretatorImpl createInterpretator() {
        return new InterpretatorHoover();
    }

    @Override
    protected void createToolBarButtons() {
        bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

        JPanel panel = getjPanel();

        createButton("left", panel);
        createButton("right", panel);
        createButton("up", panel);
        createButton("down", panel);

        createButton("put", panel);
        createButton("take", panel);
        createButton("save", panel);

        createButton("procedure", panel);
        createButton("if", panel);
        createButton("else", panel);
        createButton("while", panel);
        createButton("do", panel);
        createButton("end", panel);

        createButton("can_left", panel);
        createButton("can_right", panel);
        createButton("can_up", panel);
        createButton("can_down", panel);

        createButton("is_bag_empty", panel);
        createButton("is_bag_not_empty", panel);
        createButton("is_cell_empty", panel);
        createButton("is_cell_not_empty", panel);

        createButton("run", panel);
    }

    public abstract void onLeftClick(String param);

    public abstract void onRightClick(String param);

    public abstract void onRunButtonAction();

    private void createButton(final String name, JPanel panel) {
        JButton button = new JButton(bundle.getString(name));
        button.setAlignmentX(Component.LEFT_ALIGNMENT);

        if ("left".equals(name) || "right".equals(name) || "up".equals(name) || "down".equals(name) || "put".equals(name) || "take".equals(name) || "save".equals(name)) {
            button.setBackground(new Color(198, 226, 255));
        } else {
            button.setBackground(Color.WHITE);
        }

        if ("save".equals(name) || "end".equals(name) || "can_down".equals(name) || "is_cell_not_empty".equals(name)) {
            panel.add(button, "wrap");
        } else {
            panel.add(button, "split");
        }
        if ("run".equals(name)) {
            run = button;
            button.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    onRunButtonAction();
                }
            });
            button.setBackground(new Color(60, 179,	113));
            return;
        }
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    onLeftClick(name);
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    onRightClick(name);
                }
            }
        });
    }

    private JPanel getjPanel() {
        JPanel topPane = new JPanel(null);
        topPane.setLayout(new MigLayout());
        topPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        getToolbarField().add(topPane);
        return topPane;
    }
}
