package com.yme.performer.forms.hoover;

import java.util.ResourceBundle;

public class MainFormViewHooverController extends MainFormViewHooverBuilder {
    private final ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

    public MainFormViewHooverController() {
        super();
        refreshCodeFieldStyles();
    }

    @Override
    public void onLeftClick(String param) {
        insertCodeIntoCursor(bundle.getString(param) + "\n");
    }

    @Override
    public void onRightClick(String param) {
        getGameField().onCallback(param, null);
    }

    @Override
    public void onRunButtonAction() {
        executePerformer();
    }

    @Override
    protected String getDefaultCode() {
//        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");
//
//        return (
//                bundle.getString("left") + "\n"
//                        + bundle.getString("take") + "\n"
//        ).toLowerCase();
        return "Візьми\n" +
                "Вправо\n" +
                "Візьми\n" +
                "Вправо\n" +
                "Візьми\n" +
                "Вправо\n" +
                "Візьми\n" +
                "Вправо\n" +
                "Візьми\n" +
                "Вправо\n" +
                "Візьми\n" +
                "Вниз\n" +
                "Візьми\n" +
                "Вліво\n" +
                "Візьми\n" +
                "Вліво\n" +
                "Візьми\n" +
                "Вліво\n" +
                "Візьми\n" +
                "Вліво\n" +
                "Візьми\n" +
                "Вліво\n" +
                "\n" +
                "Доки відсік не порожній повторяти\n" +
                "  поклади\n" +
                "  якщо можна вправо\n" +
                "    вправо\n" +
                "  інакше\n" +
                "    якщо можна вниз\n" +
                "      вниз\n" +
                "    кінець\n" +
                "    доки можна вліво повторяти\n" +
                "      вліво\n" +
                "    кінець\n" +
                "  кінець\n" +
                "кінець";
    }
}
