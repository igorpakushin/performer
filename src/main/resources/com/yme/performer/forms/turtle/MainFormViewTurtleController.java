package com.yme.performer.forms.turtle;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainFormViewTurtleController extends MainFormViewTurtleBuilder {

    public MainFormViewTurtleController() {
        super();

        refreshCodeFieldStyles();
    }

    @Override
    protected String getDefaultCode() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        return (
                "repeat 72 [\n" +
                "     " + bundle.getString("forward") + " 5\n" +
                "     " + bundle.getString("right") + " 5\n" +
                "]\n"
        ).toLowerCase();
    }

    @Override
    public void onRunButtonAction() {
        executePerformer();
    }
}
