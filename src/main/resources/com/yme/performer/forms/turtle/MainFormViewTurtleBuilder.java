package com.yme.performer.forms.turtle;

import com.yme.performer.forms.MainFormViewBase;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.interpretator.turtle.InterpretatorTurtle;
import com.yme.performer.ui.component.turtle.GameFieldTurtleController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

public abstract class MainFormViewTurtleBuilder extends MainFormViewBase {

    static java.util.List<String> keywords;

    public MainFormViewTurtleBuilder() {
        super();

        setGameField(new GameFieldTurtleController());

        Dimension requiredSize = getGameField().getRequiredSize();
        requiredSize.width *= 1.5;  // codeField will be 50% paintPanel wide
        Dimension toolbarFieldPreferredSize = getToolbarField().getPreferredSize();
        requiredSize.height += toolbarFieldPreferredSize.height + 50;
        setSize(requiredSize);

        setTitle("Main Form Turtle");

        final ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");
        keywords  = new ArrayList<String>() {{
            //# movement
            add(bundle.getString("forward"));
            add(bundle.getString("back"));
            add(bundle.getString("right"));
            add(bundle.getString("left"));

            //#drwing
            add(bundle.getString("draw"));
            add(bundle.getString("nodraw"));
            add(bundle.getString("color"));
            add(bundle.getString("cls"));
            add(bundle.getString("stop"));

            // other keywords
            add(bundle.getString("to"));
            add(bundle.getString("if"));
            add(bundle.getString("repeat"));
            add(bundle.getString("end"));
        }};
    }

    @Override
    protected List<String> getKeywordsList() {
        return keywords;
    }

    @Override
    protected InterpretatorImpl createInterpretator() {
        return new InterpretatorTurtle();
    }

    @Override
    protected void createToolBarButtons()
    {
        JPanel topPane = getjPanel();
        JPanel botPane = getjPanel();

        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        run = getjButton(bundle.getString("run"));

        botPane.add(run);

        createListeners(run);
    }

    public abstract void onRunButtonAction();

    private void createListeners(JButton run) {
        final MainFormViewTurtleBuilder me = this;

        run.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                me.onRunButtonAction();
            }
        });

    }

    private JButton getjButton(String text) {
        JButton jump = new JButton(text);
        jump.setAlignmentX(Component.LEFT_ALIGNMENT);
        return jump;
    }

    private JPanel getjPanel() {
        JPanel topPane = new JPanel(null);
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.X_AXIS));
        topPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        getToolbarField().add(topPane);
        return topPane;
    }
}
