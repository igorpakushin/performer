package com.yme.performer.forms;

import com.yme.performer.interpretator.Interpretator;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.ui.component.GameField;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public abstract class MainFormViewBase extends JFrame {

    private Interpretator interpretator;

    private JPanel toolbarField;
    private JPanel panelContent;
    private JPanel mainContainer;
    private JTextPane codeField;
    protected JButton run;

    private JMenuItem runMenuItem;
    private JMenuItem terminateMenuItem;

    private GameField gameField;
    private Thread performerThread;

    public MainFormViewBase() {
        interpretator = createInterpretator();

        setTitle("MainForm");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(panelContent);
        createToolBarButtons();
        codeField.setText(getDefaultCode());

        toolbarField.setLayout(new BoxLayout(toolbarField, BoxLayout.Y_AXIS));
        mainContainer.setLayout(new BoxLayout(mainContainer, BoxLayout.X_AXIS));
        mainContainer.add(Box.createRigidArea(new Dimension(5, 0)), 1); // spacer

        codeField.addStyle("keyword", null);
        codeField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {}

            @Override
            public void keyReleased(KeyEvent e) {
                if (!e.isActionKey())
                    refreshCodeFieldStyles();
            }
        });

        createMenu();

        setVisible(true);
    }

    public void insertCodeIntoCursor(String codeToInsert)
    {
        StyledDocument doc = codeField.getStyledDocument();

        try {
            doc.insertString(codeField.getCaretPosition(), codeToInsert, null);
            refreshCodeFieldStyles();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    protected void createMenu()
    {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        //Create the menu bar.
        menuBar = new JMenuBar();

        //Build the file menu.
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");
        menuBar.add(menu);

        //a group of JMenuItems
        menuItem = new JMenuItem("Open", KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        //menuItem.getAccessibleContext().setAccessibleDescription("This doesn't really do anything");
        menu.add(menuItem);

        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showOpenDialog(null);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();

                    String contentsOfFile = null;
                    try {
                        contentsOfFile = readFileAsString(file.getAbsolutePath());
                        codeField.setText(contentsOfFile);
                        refreshCodeFieldStyles();
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(null, "Ops! Couldn't read contents of file");
                    }
                }
            }
        });

        menuItem = new JMenuItem("Save as...", KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        menu.add(menuItem);

        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser filesave = new JFileChooser();
                int retrival = filesave.showSaveDialog(null);
                if (retrival == filesave.APPROVE_OPTION) {
                    File file = filesave.getSelectedFile();

                    try {
                        PrintWriter out = new PrintWriter(file.getAbsolutePath(), "UTF-8"/*Charset.defaultCharset().toString()*/);
                        out.write(codeField.getText());
                        out.close();
                    } catch (FileNotFoundException e1) {
                        JOptionPane.showMessageDialog(null, "Ops! Couldn't save file");
                    } catch (UnsupportedEncodingException e1) {
                        JOptionPane.showMessageDialog(null, "Ops! Couldn't save file, unknown charset");
                    }

                }
            }
        });


        //Build the program menu.
        menu = new JMenu("Program");
        menu.setMnemonic(KeyEvent.VK_A);
        menu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");
        menuBar.add(menu);

        //a group of JMenuItems
        runMenuItem = new JMenuItem("Run", KeyEvent.VK_T);
        runMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, ActionEvent.CTRL_MASK));
        menu.add(runMenuItem);

        runMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                run.doClick();
            }
        });

        terminateMenuItem = new JMenuItem("Terminate", KeyEvent.VK_T);
        terminateMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
        menu.add(terminateMenuItem);

        terminateMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performerThread.stop();
                enableControlsAfterExecution();
            }
        });

        setJMenuBar(menuBar);
    }

    private String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    protected void setBoldStyleSettings(Style style)
    {
        StyleConstants.setForeground(style, Color.blue);
        StyleConstants.setBold(style, true);
    }

    protected void setDefaultStyleSettings(Style style)
    {
        StyleConstants.setForeground(style, Color.black);
        StyleConstants.setBold(style, false);
    }

    private void cleanUpMarkup(StyledDocument doc, Style style) throws BadLocationException {
        int caretPosition = codeField.getCaretPosition();
        int selectionStart = codeField.getSelectionStart();
        int selectionEnd = codeField.getSelectionEnd();

        String str = doc.getText(0, doc.getLength()); // get text contents
        doc.remove(0, doc.getLength());               // remove contents to clear markup
        doc.insertString(0, str, style);              // add text back to update markup

        codeField.setCaretPosition(caretPosition);
        codeField.setSelectionStart(selectionStart);
        codeField.setSelectionEnd(selectionEnd);
    }

    private boolean isWhitespaceChar(char ch)
    {
        return ch == ' ' || ch == '\n' || ch == '\t';
    }

    private void highlightLine(int errorLineNumber) {

        StyledDocument doc = codeField.getStyledDocument();

        String str = null;
        try {
            str = doc.getText(0, doc.getLength()).toLowerCase();
        } catch (BadLocationException e) {
            e.printStackTrace();
            return;
        }

        int start = 0;
        if (errorLineNumber > 1)
            for (int i=0; i<str.length(); i++) {
                if (str.charAt(i) == '\n') start++;
                if (start == errorLineNumber-1) {
                    start = i+1;
                    break;
                }
            }

        int end = 0;
        for (int i=start; i<str.length(); i++)
            if (str.charAt(i) == '\n') {
                end = i;
                break;
            }

        setBoldStyleSettings(codeField.getStyle("keyword"));    // set styles to highlight the code
        StyleConstants.setBackground(codeField.getStyle("keyword"), Color.pink);
        doc.setCharacterAttributes(start, end-start, codeField.getStyle("keyword"), false); // highlight a line
        StyleConstants.setBackground(codeField.getStyle("keyword"), Color.white);
        setDefaultStyleSettings(codeField.getStyle("keyword")); // reset back to default styles
    }

    private void highlightOccurencesOfKeyword(StyledDocument doc, String findStr) throws BadLocationException {
        int lastIndex = 0;
        String str = doc.getText(0, doc.getLength()).toLowerCase();
        findStr = findStr.toLowerCase();

        // search for occurrences of the given word and highlight it
        setBoldStyleSettings(codeField.getStyle("keyword"));    // set styles to highlight the code
        while(lastIndex != -1)
        {
            lastIndex = str.indexOf(findStr,lastIndex);
            if(lastIndex != -1)
            {
                // do not highlight if no whitespace character before the found word
                if (lastIndex > 0 && !isWhitespaceChar(str.charAt(lastIndex-1)))
                {
                    lastIndex+=findStr.length();
                    continue;
                }

                // do not highlight if no whitespace character after the found word
                if (lastIndex+findStr.length() < str.length() &&  !isWhitespaceChar(str.charAt(lastIndex+findStr.length())))
                {
                    lastIndex+=findStr.length();
                    continue;
                }

                // highlight a keyword
                doc.setCharacterAttributes(lastIndex, findStr.length(), codeField.getStyle("keyword"), false);
                lastIndex+=findStr.length();
            }
        }
        setDefaultStyleSettings(codeField.getStyle("keyword")); // reset back to default styles

    }

    protected void refreshCodeFieldStyles() {
        try {
            StyledDocument doc = codeField.getStyledDocument();
            cleanUpMarkup(doc, codeField.getStyle("keyword"));   // remove previously highlighted code

            List<String> keywords = getKeywordsList();
            for (String keyword : keywords)
                highlightOccurencesOfKeyword(doc, keyword);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

    }

    abstract protected java.util.List<String> getKeywordsList();

    protected InterpretatorImpl createInterpretator() {
        return new InterpretatorImpl();
    }

    protected String getDefaultCode() {
        return "";
    }

    protected void createToolBarButtons() {
    }

    public boolean onCallback(String method, String[] args) {
        System.out.println("Hi");

        return false;
    }

    public JPanel getToolbarField() {
        return toolbarField;
    }

    public JTextPane getCodeField() {
        return codeField;
    }

    public Interpretator getInterpretator() {
        return interpretator;
    }

    public GameField getGameField() {
        return gameField;
    }

    public void setGameField(GameField gameField) {
        mainContainer.remove(this.gameField);
        this.gameField = gameField;
        mainContainer.add(gameField);
    }

    protected void disableControlsWhileExecuting()
    {
        run.setEnabled(false);
        runMenuItem.setEnabled(false);
    }

    protected void enableControlsAfterExecution()
    {
        run.setEnabled(true);
        runMenuItem.setEnabled(true);
    }

    protected void onBeforeExecution()
    {
        disableControlsWhileExecuting();
    }

    protected void onAfterExecution(boolean runtime_error)
    {
        enableControlsAfterExecution();
    }

    protected void onError(int errorLineNumber)
    {
        JOptionPane.showMessageDialog(null, "Error in line " + getInterpretator().getErrorLineNumber());

        highlightLine(getInterpretator().getErrorLineNumber());
    }

    protected void executePerformer() {
        performerThread = new Thread(new HookOnDestroy(
            new Runnable() { // normal run
                @Override
                public void run() {
                    onBeforeExecution();

                    Interpretator.ErrorCode errorCode = getInterpretator().execute(getGameField(), getCodeField().getText().toLowerCase());
                    if (errorCode != Interpretator.ErrorCode.SUCCESS)
                        onError(getInterpretator().getErrorLineNumber());

                    onAfterExecution(false);
                }
            },
            new Runnable() { // interruption
                @Override
                public void run() {
                    onAfterExecution(true);
                }
            }
        ));
        performerThread.start();
    }

    // helper class to hook thread interruption
    private final class HookOnDestroy implements Runnable {
        private final Runnable action;
        private final Runnable hook;
        public HookOnDestroy(Runnable action, Runnable hook) {
            this.hook = hook;
            this.action = action;
        }

        @Override
        public void run() {
            try {
                action.run();
            } catch (Exception e){
                hook.run();
            }
        }

    }
}
//15x19, 6x8, hz
