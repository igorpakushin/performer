package com.yme.performer.forms.roo;

import com.yme.performer.forms.MainFormViewBase;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.interpretator.roo.InterpretatorRoo;
import com.yme.performer.ui.component.roo.GameFieldRooController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.List;

public abstract class MainFormViewRooBuilder extends MainFormViewBase {

    static java.util.List<String> keywords;

    public MainFormViewRooBuilder() {
        super();

        setGameField(new GameFieldRooController());

        Dimension requiredSize = getGameField().getRequiredSize();
        requiredSize.width *= 1.5;  // codeField will be 50% paintPanel wide
        Dimension toolbarFieldPreferredSize = getToolbarField().getPreferredSize();
        requiredSize.height += toolbarFieldPreferredSize.height + 50;
        setSize(requiredSize);

        setTitle("Main Form Roo");

        final ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");
        keywords  = new ArrayList<String>() {{
            add(bundle.getString("step"));
            add(bundle.getString("jump"));
            add(bundle.getString("rotate"));
            add(bundle.getString("procedure"));
            add(bundle.getString("is_border_ahead"));
            add(bundle.getString("is_no_border_ahead"));
        }};
    }

    @Override
    protected List<String> getKeywordsList() {
        return keywords;
    }

    @Override
    protected InterpretatorImpl createInterpretator() {
        return new InterpretatorRoo();
    }

    @Override
    protected void createToolBarButtons()
    {
        JPanel topPane = getjPanel();
        JPanel botPane = getjPanel();

        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        JButton jump = getjButton(bundle.getString("jump"));
        JButton step = getjButton(bundle.getString("step"));
        JButton rotate = getjButton(bundle.getString("rotate"));
        run = getjButton(bundle.getString("run"));

        topPane.add(jump);
        topPane.add(step);
        topPane.add(rotate);
        botPane.add(run);

        createListeners(jump, step, rotate, run);
    }

    public abstract void onJumpButtonAction();

    public abstract void onStepButtonAction();

    public abstract void onRotateButtonAction();

    public abstract void onRunButtonAction();

    private void createListeners(JButton jump, JButton step, JButton rotate, JButton run) {
        final MainFormViewRooBuilder me = this;
        jump.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                me.onJumpButtonAction();
            }
        });

        step.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                me.onStepButtonAction();
            }
        });
        rotate.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                me.onRotateButtonAction();
            }
        });
        run.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                me.onRunButtonAction();
            }
        });

    }

    private JButton getjButton(String text) {
        JButton jump = new JButton(text);
        jump.setAlignmentX(Component.LEFT_ALIGNMENT);
        return jump;
    }

    private JPanel getjPanel() {
        JPanel topPane = new JPanel(null);
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.X_AXIS));
        topPane.setAlignmentX(Component.LEFT_ALIGNMENT);
        getToolbarField().add(topPane);
        return topPane;
    }
}
