package com.yme.performer.forms.roo;

import javax.swing.*;
import java.util.ResourceBundle;

public class MainFormViewRooController extends MainFormViewRooBuilder {

    private final ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

    public MainFormViewRooController() {
        super();

        refreshCodeFieldStyles();
    }

    @Override
    protected String getDefaultCode() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        return (
                bundle.getString("step") + "\n" +
                        bundle.getString("rotate") + "\n" +
                        bundle.getString("jump") + "\n"
        ).toLowerCase();
    }

    @Override
    public void onJumpButtonAction() {
        getGameField().onCallback("jump", null);
        insertCodeIntoCursor(bundle.getString("jump") + "\n");
    }

    @Override
    public void onStepButtonAction() {
        getGameField().onCallback("step", null);
        insertCodeIntoCursor(bundle.getString("step") + "\n");
    }

    @Override
    public void onRotateButtonAction() {
        getGameField().onCallback("rotate", null);
        insertCodeIntoCursor(bundle.getString("rotate") + "\n");
    }

    @Override
    public void onRunButtonAction() {
        executePerformer();
    }

    @Override
    protected void onAfterExecution(boolean runtime_error) {
        enableControlsAfterExecution();
        if (runtime_error)
            JOptionPane.showMessageDialog(null, "Ops! Border reached!");
    }
}
