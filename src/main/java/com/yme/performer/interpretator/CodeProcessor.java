package com.yme.performer.interpretator;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class CodeProcessor {
    final static String HEADING =
            "\n" +
                    "require 'java'\n" +
                    "def execute(callback)\n" +
                    "    java_import 'com.yme.performer.CallbackListener'\n\n" +
                    "    \n" +
                    "    @callback = callback\n";
    final static String FOOTER =
            "\n\n" +
                    "    return 0\n" + // FIXME: should return success or smth
                    "end\n";

    public String wrapUp(String code) {
        String result = HEADING + code + FOOTER;
        // Log.info(result);
        return result;
    }

    public String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public String decode(String code) {
        return code;
    }

}
