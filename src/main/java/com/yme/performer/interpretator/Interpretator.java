package com.yme.performer.interpretator;

import com.yme.performer.CallbackListener;

public interface Interpretator {
    public enum ErrorCode {
        SUCCESS,
        EXECUTION_ERROR,
        UNKNOWN_ERROR,
        SCRIPT_EXCEPTION
    }

    public ErrorCode execute(CallbackListener listener, String code);

    public int getErrorLineNumber();
}
