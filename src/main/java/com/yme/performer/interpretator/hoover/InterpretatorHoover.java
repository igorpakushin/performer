package com.yme.performer.interpretator.hoover;

import com.yme.performer.interpretator.CodeProcessor;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.interpretator.hoover.CodeProcessorHoover;

public class InterpretatorHoover extends InterpretatorImpl {

    @Override
    protected CodeProcessor createCodeProcessor() {
        return new CodeProcessorHoover();
    }
}
