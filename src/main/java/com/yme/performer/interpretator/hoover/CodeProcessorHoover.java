package com.yme.performer.interpretator.hoover;

import com.ibm.icu.text.Transliterator;
import com.yme.performer.interpretator.CodeProcessor;

import java.util.ResourceBundle;

public class CodeProcessorHoover extends CodeProcessor {

    @Override
    public String decode(String code) {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

        // basic commands
        String left = bundle.getString("left").toLowerCase();
        String right = bundle.getString("right").toLowerCase();
        String up = bundle.getString("up").toLowerCase();
        String down = bundle.getString("down").toLowerCase();

        String take = bundle.getString("take").toLowerCase();
        String put = bundle.getString("put").toLowerCase();
        String save = bundle.getString("save").toLowerCase();

        // conditional commands
        String can_left = bundle.getString("can_left").toLowerCase();
        String can_right = bundle.getString("can_right").toLowerCase();
        String can_up = bundle.getString("can_up").toLowerCase();
        String can_down = bundle.getString("can_down").toLowerCase();

        String is_bag_empty = bundle.getString("is_bag_empty").toLowerCase();
        String is_bag_not_empty = bundle.getString("is_bag_not_empty").toLowerCase();
        String is_cell_empty = bundle.getString("is_cell_empty").toLowerCase();
        String is_cell_not_empty = bundle.getString("is_cell_not_empty").toLowerCase();

        // other commands
        String procedure = bundle.getString("procedure").toLowerCase();
        String operator_if = bundle.getString("if").toLowerCase();
        String operator_else = bundle.getString("else").toLowerCase();
        String operator_while = bundle.getString("while").toLowerCase();
        String operator_do = bundle.getString("do").toLowerCase();
        String end = bundle.getString("end").toLowerCase();

        String rules = String.format(
                // basic commands
                "%s > '@callback.onCallback(''left'', [])'; " +
                "%s > '@callback.onCallback(''right'', [])'; " +
                "%s > '@callback.onCallback(''up'', [])'; " +
                "%s > '@callback.onCallback(''down'', [])'; " +

                "%s > '@callback.onCallback(''take'', [])'; " +
                "%s > '@callback.onCallback(''put'', [])'; " +
                "%s > '@callback.onCallback(''save'', [])'; " +

                // conditional commands
                "'%s' > '@callback.onCallback(''can_left'', [])'; " +
                "'%s' > '@callback.onCallback(''can_right'', [])'; " +
                "'%s' > '@callback.onCallback(''can_up'', [])'; " +
                "'%s' > '@callback.onCallback(''can_down'', [])'; " +
                "'%s' > '@callback.onCallback(''is_bag_empty'', [])'; " +
                "'%s' > '@callback.onCallback(''is_bag_not_empty'', [])'; " +
                "'%s' > '@callback.onCallback(''is_cell_empty'', [])'; " +
                "'%s' > '@callback.onCallback(''is_cell_not_empty'', [])'; " +

                // other
                "'%s' > 'def '; " +
                "'%s' > 'if '; " +
                "'%s' > 'else '; " +
                "'%s' > 'while '; " +
                "'%s' > 'do '; " +
                "'%s' > 'end'; " +

                ":: any-latin ;",
                left, right, up, down, take, put, save, // basic commands
                can_left, can_right, can_up, can_down, is_bag_empty, is_bag_not_empty, is_cell_empty, is_cell_not_empty, // conditional commands
                procedure, operator_if, operator_else, operator_while, operator_do, end);

        Transliterator transliterator = Transliterator.createFromRules("transliterator", rules, Transliterator.FORWARD);
        String result = transliterator.transform(code);

        result = deAccent(result);

        return result;
    }
}