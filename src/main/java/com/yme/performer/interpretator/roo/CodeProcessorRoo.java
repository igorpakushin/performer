package com.yme.performer.interpretator.roo;

import com.ibm.icu.text.Transliterator;
import com.yme.performer.interpretator.CodeProcessor;

import java.text.Normalizer;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class CodeProcessorRoo extends CodeProcessor {

    @Override
    public String decode(String code) {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        String step = bundle.getString("step").toLowerCase();
        String jump = bundle.getString("jump").toLowerCase();
        String rotate = bundle.getString("rotate").toLowerCase();
        String procedure = bundle.getString("procedure").toLowerCase();
        String end = bundle.getString("end").toLowerCase();
        String loop = bundle.getString("while").toLowerCase();

        String is_border_ahead = bundle.getString("is_border_ahead").toLowerCase();
        String is_no_border_ahead = bundle.getString("is_no_border_ahead").toLowerCase();

        String rules = String.format(
                "%s > '@callback.onCallback(''step'', [])'; " +
                        "%s > '@callback.onCallback(''jump'', [])'; " +
                        "%s > '@callback.onCallback(''rotate'', [])'; " +
                        "%s > def; " +
                        "%s > end; " +
                        "%s > while; " +
                        "'%s' > '@callback.onCallback(''is_border_ahead'', [])'; " +
                        "'%s' > '@callback.onCallback(''is_no_border_ahead'', [])'; " +
                        ":: any-latin ;",
                step, jump, rotate, procedure, end, loop, is_border_ahead, is_no_border_ahead);

        Transliterator transliterator = Transliterator.createFromRules("transliterator", rules, Transliterator.FORWARD);
        String result = transliterator.transform(code);

        result = deAccent(result);

        return result;
    }
}