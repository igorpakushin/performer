package com.yme.performer.interpretator.roo;

import com.yme.performer.interpretator.CodeProcessor;
import com.yme.performer.interpretator.InterpretatorImpl;
import com.yme.performer.interpretator.roo.CodeProcessorRoo;

public class InterpretatorRoo extends InterpretatorImpl {

    @Override
    protected CodeProcessor createCodeProcessor() {
        return new CodeProcessorRoo();
    }
}
