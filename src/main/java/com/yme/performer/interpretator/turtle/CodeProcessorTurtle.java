package com.yme.performer.interpretator.turtle;

import com.ibm.icu.text.RuleBasedTransliterator;
import com.ibm.icu.text.Transliterator;
import com.yme.performer.interpretator.CodeProcessor;

import java.util.ResourceBundle;
import java.util.logging.Logger;

public class CodeProcessorTurtle extends CodeProcessor {

    @Override
    public String decode(String code) {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // movement commands
        String forward = bundle.getString("forward").toLowerCase();
        String back = bundle.getString("back").toLowerCase();
        String right = bundle.getString("right").toLowerCase();
        String left = bundle.getString("left").toLowerCase();

        // drawing commands
        String draw = bundle.getString("draw").toLowerCase();
        String nodraw = bundle.getString("nodraw").toLowerCase();
        String color = bundle.getString("color").toLowerCase();
        String cls = bundle.getString("cls").toLowerCase();
        String stop = bundle.getString("stop").toLowerCase();

        String variables = String.format(
            "$var=[a-zA-Z][a-zA-Z0-9_]*;" +
            "':'($var) ' '* (':'$var) > $1\\,|$2;" + // separate function arguments with commas
            "':'($var) > $1; "
        );

        Transliterator transliterator = RuleBasedTransliterator.createFromRules("transliterator", variables, Transliterator.FORWARD);
        String result = transliterator.transform(code);


        String rules = String.format(
            // variables
            "$iterator=i\\_inner\\_iterator\\_19\\_5\\_19\\_03;" +
            "$num=[0-9]*;" +
            "$var=[a-zA-Z][a-zA-Z0-9_]*;" +
            "$varnum=' '* ($var)*($num)*;" +
            "$operation=' '* [+-\\/\\*];" +
            "$comparison=[<>=!]([=])*;" +
            "$parenthesis=' '* (\\($varnum ($operation $varnum)* ' '* \\))*;" +
            "$argument=$varnum ($operation $varnum)* $parenthesis ' '* ($operation $varnum)* $parenthesis ;" +

            // procedures
            "'to ' > 'def ';" +

            // if statement
            "'if '($argument) '=' ($argument) '[' > 'if '$1' == '$2' then '; " +
            "'if '($argument) '<>' ($argument) '[' > 'if '$1' != '$2' then '; " +
            "'if '($argument) ($comparison) ($argument) '[' > 'if '$1' '$2' '$3' then '; " +
            "']' ' '* '[' > ' else '; " +

            //repeat loop
            "']' > 'end';" +
            "repeat ($argument) ' '* '[' > 'for '$iterator' in (1..'$1') do ';" +
            "repeat ($num)      ' '* '[' > 'for '$iterator' in (1..'$1') do ';" +

            "':'($var) > $1; " +
            // movement commands
            "'%s '($argument) > '@callback.onCallback(''forward'', [\"#{'$1'}\"]);'; " +
            "'%s '($argument) > '@callback.onCallback(''back'', [\"#{'$1'}\"]);'; " +
            "'%s '($argument) > '@callback.onCallback(''right'', [\"#{'$1'}\"]);'; " +
            "'%s '($argument) > '@callback.onCallback(''left'', [\"#{'$1'}\"]);'; " +

            //drawing commands
            "%s               > '@callback.onCallback(''draw'', []);'; " +
            "%s               > '@callback.onCallback(''nodraw'', []);'; " +
            "'%s '($argument) > '@callback.onCallback(''color'', [\"#{'$1'}\"]);'; " +
            "%s               > '@callback.onCallback(''cls'', []);'; " +
            "%s               > '@callback.onCallback(''stop'', []);'; " +

            ":: any-latin ;",
        forward, back, right, left,         // movement commands
        draw, nodraw, color, cls, stop      // drawing commands
        );


        transliterator = RuleBasedTransliterator.createFromRules("transliterator", rules, Transliterator.FORWARD);
        result = transliterator.transform(result);

        result = deAccent(result);

        Logger log = Logger.getLogger("logger");
        log.info(result);

        return result;
    }
}