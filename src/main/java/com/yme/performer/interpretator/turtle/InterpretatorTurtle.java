package com.yme.performer.interpretator.turtle;

import com.yme.performer.interpretator.CodeProcessor;
import com.yme.performer.interpretator.InterpretatorImpl;

public class InterpretatorTurtle extends InterpretatorImpl {

    @Override
    protected CodeProcessor createCodeProcessor() {
        return new CodeProcessorTurtle();
    }
}
