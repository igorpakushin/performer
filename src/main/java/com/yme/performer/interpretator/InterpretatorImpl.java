package com.yme.performer.interpretator;

import com.yme.performer.CallbackListener;
import org.jruby.exceptions.RaiseException;

import javax.script.*;
import java.io.*;

public class InterpretatorImpl implements Interpretator {

    private final ScriptEngine jruby;
    private CodeProcessor codeProcessor;
    private int errorLineNumber;

    public InterpretatorImpl() {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        jruby = scriptEngineManager.getEngineByExtension("rb");

        codeProcessor = createCodeProcessor();
    }

    protected CodeProcessor createCodeProcessor() {
        return new CodeProcessor();
    }

    private void handleException(Exception e)
    {
        Throwable cause = e.getCause();
        while (!RaiseException.class.isInstance(cause))
            cause = cause.getCause();
        StackTraceElement[] stackTraceElement = cause.getStackTrace();
        if (stackTraceElement.length > 0) errorLineNumber = stackTraceElement[0].getLineNumber();
        else
        {
            String num = cause.getMessage().substring(cause.getMessage().indexOf(':') + 1);
            num = num.substring(0, num.indexOf(':'));
            errorLineNumber = Integer.parseInt(num);
        }
    }

    @Override
    public ErrorCode execute(CallbackListener listener, String code) {
        code = codeProcessor.wrapUp(codeProcessor.decode(code));
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(code.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return ErrorCode.UNKNOWN_ERROR;
        }

        ErrorCode result = ErrorCode.SUCCESS;
        Reader reader = new InputStreamReader(is);
        try {
            jruby.eval(reader);
            Invocable invocableEngine = (Invocable) jruby;
            invocableEngine.invokeFunction("execute", listener);
        } catch (ScriptException e) {
            handleException(e);
            result = ErrorCode.SCRIPT_EXCEPTION;
        }
        catch (NoSuchMethodException e) {
            handleException(e);
            result = ErrorCode.UNKNOWN_ERROR;
        }
        catch (NullPointerException e) {
            // this happends upon performer thread interruption:
            //      thread is being interrupted and no value is returned to script engine
        }
        errorLineNumber -= 7;

        return result;
    }

    @Override
    public int getErrorLineNumber() {
        return errorLineNumber;
    }
}
