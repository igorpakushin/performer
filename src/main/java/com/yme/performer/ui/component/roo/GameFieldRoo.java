package com.yme.performer.ui.component.roo;

import com.yme.performer.ui.component.GameField;
import com.yme.performer.ui.executor.PerformerRoo;

import java.awt.*;

public class GameFieldRoo extends GameField {
    private static final int CELL_SIZE = 30;
    private static final int FIELD_WIDTH = 15;
    private static final int FIELD_HEIGHT = 19;

    public GameFieldRoo() {
        super();
        setBackground(Color.WHITE);
        setPreferredSize(getRequiredSize());
        setMinimumSize(getRequiredSize());
        setMaximumSize(getRequiredSize());
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawCells(g);
        getRoo().draw(g, getCellSize());
    }

    private void drawCells(Graphics g) {
        g.setColor(Color.YELLOW);
        for (int i = 0; i <= getFieldHeight(); i ++) {
            g.drawLine(getCellSize() * 2, (i + 2) * getCellSize(), (getFieldWidth() + 2) * getCellSize(), (i + 2) * getCellSize());
        }
        for (int i = 0; i <= getFieldWidth(); i ++) {
            g.drawLine((i + 2) * getCellSize(), getCellSize() * 2, (i + 2) * getCellSize(), (getFieldHeight() + 2) * getCellSize());
        }
    }

    @Override
    public Dimension getRequiredSize() {
        return new Dimension((FIELD_WIDTH + 4) * getCellSize() + 1, (FIELD_HEIGHT + 4) * getCellSize() + 1);
    }

    public static int getCellSize() {
        return CELL_SIZE;
    }

    public PerformerRoo getRoo() {
        return null;
    }

    public static int getFieldWidth() {
        return FIELD_WIDTH;
    }

    public static int getFieldHeight() {
        return FIELD_HEIGHT;
    }
}
