package com.yme.performer.ui.component.hoover;

import java.awt.image.BufferedImage;

public interface Trash {
    public String getName();

    public BufferedImage getImage();

    public String getDisplayName();
}