package com.yme.performer.ui.component.hoover;

import com.yme.performer.ui.component.GameField;
import com.yme.performer.ui.executor.PerformerHoover;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;

public class GameFieldHoover extends GameField {
    private static final int CELL_SIZE = 51;
    protected static final int FIELD_WIDTH = 6;
    protected static final int FIELD_HEIGHT = 8;
    private static final int TOP_FIELD_HEIGHT = 2;

    private BufferedImage monitor;
    private BufferedImage printer;

    public GameFieldHoover() {
        super();
        setBackground(Color.WHITE);
        setPreferredSize(getRequiredSize());
        setMinimumSize(getRequiredSize());
        setMaximumSize(getRequiredSize());
        initImages();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawField(g);
    }

    private void drawField(Graphics g) {
        drawTopField(g);
        drawCenterField(g);
        drawRightField(g);
        drawHoover(g);
    }

    private void drawTopField(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, getFieldWidth() * getCellSize(), TOP_FIELD_HEIGHT * getCellSize());
        g.drawImage(monitor, 15, 0, null);
        g.drawImage(printer, 175, 0, null);
        Trash trash = getHoover().getSavedTrash();
        if (trash != null) {
            g.drawImage(trash.getImage(), 40, 15, null);
            g.setColor(Color.blue);
            g.setFont(getFont().deriveFont(Font.BOLD));
            g.drawString(trash.getDisplayName(), 210, 90);
        }
    }

    private void drawCenterField(Graphics g) {
        g.setColor(Color.BLACK);
        for (int i = TOP_FIELD_HEIGHT; i < getFieldHeight() + TOP_FIELD_HEIGHT + 1; i++) {
            g.drawLine(0, i * getCellSize(), getFieldWidth() * getCellSize(), i * getCellSize());
        }
        for (int i = 0; i < getFieldWidth() + 1; i++) {
            g.drawLine(i * getCellSize(), TOP_FIELD_HEIGHT * getCellSize(), i * getCellSize(),  (getFieldHeight() + TOP_FIELD_HEIGHT) * getCellSize());
        }

        g.setColor(Color.red);

        for (int x = 0; x < FIELD_HEIGHT; x++) {
            for (int y = 0; y < FIELD_WIDTH; y++) {
                if (getCells()[y][x] != null) {
                    g.drawImage(getCells()[y][x].getImage(), y * getCellSize() + 1, x * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + 1, null);
                }
            }
        }

    }

    private void drawHoover(Graphics g) {
        for (int i = getHoover().getY(); i < FIELD_HEIGHT; i++) {
            g.drawImage(getHoover().getImgVertical(), getHoover().getX() * getCellSize() + 16, i * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize(), null);
        }

        for (int i = getHoover().getX() + 1; i < FIELD_WIDTH; i++) {
            g.drawImage(getHoover().getImgHorizontal(), i * getCellSize() + 16, FIELD_HEIGHT * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize(), null);
        }

        g.drawImage(getHoover().getImgVH(), getHoover().getX() * getCellSize() + 16, (FIELD_HEIGHT - 1) * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize(), null);
        g.drawImage(getHoover().getImgHV(),(FIELD_WIDTH - 1) * getCellSize() + 40, (FIELD_HEIGHT - 1) * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize(), null);
        g.drawImage(getHoover().getImgHead(), getHoover().getX() * getCellSize() + 6, getHoover().getY() * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize() - 10, null);
        g.drawImage(getHoover().getImgHole(),FIELD_WIDTH * getCellSize() + 19, (FIELD_HEIGHT - 1) * getCellSize() + TOP_FIELD_HEIGHT * getCellSize() + getCellSize() - 27, null);
    }

    private void drawRightField(Graphics g) {
        g.setColor(Color.BLACK);
        int x = getFieldWidth() * getCellSize() + 10;
        int y = 0;
        int width = getCellSize();
        int height = (getFieldHeight() + TOP_FIELD_HEIGHT) * getCellSize();
        g.drawRect(x, y, width, height);

        int bottom = y + height - 5 - getCellSize();
        g.setColor(Color.red);
        int offset = 0;
        for (Trash trash : getHoover().getBag()) {
            g.drawImage(trash.getImage(), x + 1, bottom - offset, null);
            offset += getCellSize();
        }
    }

    @Override
    public Dimension getRequiredSize() {
        int border = 2;
        int freeBottomSpace = CELL_SIZE * 2;
        return new Dimension((FIELD_WIDTH + border) * getCellSize() + border, (FIELD_HEIGHT + TOP_FIELD_HEIGHT) * getCellSize() + border + freeBottomSpace);
    }

    public static int getCellSize() {
        return CELL_SIZE;
    }

    public PerformerHoover getHoover() {
        return null;
    }

    public Trash[][] getCells() {
        return null;
    }

    public static int getFieldWidth() {
        return FIELD_WIDTH;
    }

    public static int getFieldHeight() {
        return FIELD_HEIGHT;
    }

    private void initImages() {
        BufferedImage monitor = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("monitor.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover").toURI());
            monitor = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.monitor = monitor;

        BufferedImage printer = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("printer.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover").toURI());
            printer = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.printer = printer;
    }
}
