package com.yme.performer.ui.component.turtle;

public class GameFieldTurtleController extends GameFieldTurtle {

    public GameFieldTurtleController() {
        super();
    }

    @Override
    public boolean onCallback(String method, String[] args) {
        double x1 = getTurtle().getX();
        double y1 = getTurtle().getY();

        if ("forward".equals(method))
        {
            getTurtle().forward(Float.parseFloat(args[0]));
        }
        else if ("back".equals(method))
        {
            getTurtle().back(Float.parseFloat(args[0]));
        }
        else if ("right".equals(method))
        {
            getTurtle().right(Float.parseFloat(args[0]));
        }
        else if ("left".equals(method))
        {
            getTurtle().left(Float.parseFloat(args[0]));
        }

        else if ("draw".equals(method))
        {
            getTurtle().draw();
        }
        else if ("nodraw".equals(method))
        {
            getTurtle().nodraw();
        }
        else if ("color".equals(method))
        {
            getTurtle().color(Integer.parseInt(args[0]));
        }
        else if ("cls".equals(method))
        {
            getTrails().clear();
        }
        else if ("stop".equals(method))
        {
            getTurtle().stop();
        }

        double x2 = getTurtle().getX();
        double y2 = getTurtle().getY();
        if (getTurtle().isDrawFlag() && !(x1 == x2 && y1 == y2) )
            getTrails().add(new TurtleTrail(x1, y1, x2, y2, getTurtle().getColor()));

        updateUI();
        try {
            Thread.currentThread().sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
