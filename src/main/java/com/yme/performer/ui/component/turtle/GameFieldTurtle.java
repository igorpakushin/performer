package com.yme.performer.ui.component.turtle;

import com.yme.performer.ui.component.GameField;
import com.yme.performer.ui.executor.PerformerTurtle;

import java.awt.*;
import java.util.ArrayList;

public class GameFieldTurtle extends GameField
{
    private static final int BORDER = PerformerTurtle.PERFORMER_SIZE;
    private static final int FIELD_WIDTH = 612;
    private static final int FIELD_HEIGHT = 342;

    private PerformerTurtle turtle;
    private ArrayList<TurtleTrail> trails = new ArrayList<TurtleTrail>();

    public GameFieldTurtle() {
        super();
        setBackground(Color.WHITE);
        setPreferredSize(getRequiredSize());
        setMinimumSize(getRequiredSize());
        setMaximumSize(getRequiredSize());
        turtle = createPerformer();
    }

    protected PerformerTurtle createPerformer()
    {
        return new PerformerTurtle(FIELD_WIDTH/2, FIELD_HEIGHT/2);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // draw field
        g.setColor(Color.LIGHT_GRAY);
        g.drawRect(BORDER/2, BORDER/2, FIELD_WIDTH - BORDER/2, FIELD_HEIGHT - BORDER/2);

        // draw trails
        // don't use iterator here, the trails array may be altered by code processing thread
        // Iterator of ArrayList is fail fast i.e. it will crash
        // more info here:
        //      http://stackoverflow.com/questions/10690903/arraylist-concurrent-modification
        for (int i=0; i<trails.size(); i++)
        {
            g.setColor(trails.get(i).getColor());
            g.drawLine((int) trails.get(i).getX1(), (int) trails.get(i).getY1(), (int) trails.get(i).getX2(), (int) trails.get(i).getY2());
        }

        // draw performer
        getTurtle().draw(g);
    }

    @Override
    public Dimension getRequiredSize()
    {
        return new Dimension(FIELD_WIDTH + BORDER, FIELD_HEIGHT + BORDER);
    }

    public PerformerTurtle getTurtle()
    {
        return turtle;
    }

    public ArrayList<TurtleTrail> getTrails()
    {
        return this.trails;
    }

    public static int getFieldWidth()
    {
        return FIELD_WIDTH;
    }

    public static int getFieldHeight()
    {
        return FIELD_HEIGHT;
    }

    public class TurtleTrail
    {
        private double x1;
        private double y1;
        private double x2;
        private double y2;
        private Color color;

        public TurtleTrail(double x1, double y1, double x2, double y2, Color color)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
            this.color = color;
        }

        // getters
        public double getX1()
        {
            return x1;
        }

        public double getY1()
        {
            return y1;
        }

        public double getX2()
        {
            return x2;
        }

        public double getY2()
        {
            return y2;
        }

        public Color getColor()
        {
            return color;
        }
    }
}
