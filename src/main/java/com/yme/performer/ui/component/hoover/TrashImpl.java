package com.yme.performer.ui.component.hoover;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

public class TrashImpl implements Trash{
    private String name;
    private String displayName;
    private BufferedImage image;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public BufferedImage getImage() {
        return image;
    }

    public TrashImpl(final String name) {
        final ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");
        this.name = name;
        this.displayName = bundle.getString(name);
        BufferedImage image = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return (name + ".png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/trash").toURI());
            image = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.image = image;
    }
}
