package com.yme.performer.ui.component.roo;

import com.yme.performer.ui.executor.PerformerRoo;

public class GameFieldRooController extends GameFieldRoo {
    private PerformerRoo roo;

    public GameFieldRooController() {
        super();
        roo = new PerformerRoo();
    }

    @Override
    public boolean onCallback(String method, String[] args) {
        if ("step".equals(method)) {
            if (!isBorderAhead()) {
                roo.step();
            } else {
                throw new RuntimeException("Field border reached");
            }
        } else if ("rotate".equals(method)) {
            roo.rotate();
        } else if ("jump".equals(method)) {
            if (!isBorderAhead()) {
                roo.jump();
            } else {
                throw new RuntimeException("Field border reached");
            }
        } else if ("is_border_ahead".equals(method)) {
            return isBorderAhead();
        } else if ("is_no_border_ahead".equals(method)) {
            return !isBorderAhead();
        }
        updateUI();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isBorderAhead() {
        switch (roo.getDirection()) {
            case TOP:
                if (roo.getY() == 0) {
                    return true;
                }
                return false;
            case RIGHT:
                if (roo.getX() == getFieldWidth()) {
                    return true;
                }
                return false;
            case BOTTOM:
                if (roo.getY() == getFieldHeight()) {
                    return true;
                }
                return false;
            case LEFT:
                if (roo.getX() == 0) {
                    return true;
                }
                return false;
        }
        return false;
    }

    @Override
    public PerformerRoo getRoo() {
        return roo;
    }
}
