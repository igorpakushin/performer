package com.yme.performer.ui.component.hoover;

import com.yme.performer.ui.executor.PerformerHoover;

import javax.swing.*;

public class GameFieldHooverController extends GameFieldHoover {
    private PerformerHoover hoover;
    Trash[][] cells;

    public GameFieldHooverController() {
        super();
        hoover = new PerformerHoover();
        cells = new Trash[FIELD_WIDTH][FIELD_HEIGHT];
        Trash behemoth = new TrashImpl("behemoth");
        Trash cd = new TrashImpl("cd");
        Trash cubes = new TrashImpl("cubes");
        Trash key = new TrashImpl("key");
        Trash mouse = new TrashImpl("mouse");
        Trash cat = new TrashImpl("cat");
        Trash mishka = new TrashImpl("mishka");
        Trash monkey = new TrashImpl("monkey");
        Trash panda = new TrashImpl("panda");
        Trash rabbit = new TrashImpl("rabbit");
        Trash tiger = new TrashImpl("tiger");
        cells[0][0] = behemoth;
        cells[1][0] = cd;
        cells[2][0] = cubes;
        cells[3][0] = key;
        cells[4][0] = mouse;
        cells[5][0] = cat;
        cells[1][1] = mishka;
        cells[2][1] = monkey;
        cells[3][1] = panda;
        cells[4][1] = rabbit;
        cells[5][1] = tiger;
    }

    @Override
    public boolean onCallback(String method, String[] args) {
        if ("left".equals(method)) {
            left();
        } else if ("right".equals(method)) {
            right();
        } else if ("up".equals(method)) {
            up();
        } else if ("down".equals(method)) {
            down();
        } else if ("is_bag_empty".equals(method)) {
            return hoover.isBagEmpty();
        } else if ("is_bag_not_empty".equals(method)) {
            return !hoover.isBagEmpty();
        } else if ("is_cell_empty".equals(method)) {
            return isCellEmpty();
        } else if ("is_cell_not_empty".equals(method)) {
            return !isCellEmpty();
        } else if ("take".equals(method)) {
            take();
        } else if ("put".equals(method)) {
            put();
        } else if ("save".equals(method)) {
            save();
        } else if ("can_left".equals(method)) {
            return canLeft();
        } else if ("can_right".equals(method)) {
            return canRight();
        } else if ("can_up".equals(method)) {
            return canUp();
        } else if ("can_down".equals(method)) {
            return canDown();
        }
        updateUI();
        try {
            Thread.currentThread().sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public PerformerHoover getHoover() {
        return hoover;
    }

    @Override
    public Trash[][] getCells() {
        return cells;
    }

    public boolean isCellEmpty() {
        return cells[hoover.getX()][hoover.getY()] == null;
    }

    public void take() {
        if (!isCellEmpty()) {
            hoover.getBag().add(cells[hoover.getX()][hoover.getY()]);
            cells[hoover.getX()][hoover.getY()] = null;
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Порожня комірка.");
        }
    }

    public void put() {
        if (hoover.isBagEmpty()) {
            JOptionPane.showMessageDialog(null, "Помилка! Відсік порожній.");
        } else if (!isCellEmpty()) {
            JOptionPane.showMessageDialog(null, "Помилка! Комірка зайнята.");
        } else {
            cells[hoover.getX()][hoover.getY()] = hoover.getBag().get(hoover.getBag().size() - 1);
            hoover.getBag().remove(hoover.getBag().size() - 1);
        }
    }

    public void save() {
        if (!isCellEmpty()) {
            hoover.setSavedTrash(new TrashImpl(cells[hoover.getX()][hoover.getY()].getName()));
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Порожня комірка.");
        }
    }

    public boolean canLeft() {
        return hoover.getX() != 0;
    }

    public boolean canRight() {
        return hoover.getX() != FIELD_WIDTH - 1;
    }

    public boolean canUp() {
        return hoover.getY() != 0;
    }

    public boolean canDown() {
        return hoover.getY() != FIELD_HEIGHT - 1;
    }

    public void left() {
        if (canLeft()) {
            hoover.left();
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Ліворуч неможливо.");
        }
    }

    public void right() {
        if (canRight()) {
            hoover.right();
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Праворуч неможливо.");
        }
    }

    public void up() {
        if (canUp()) {
            hoover.up();
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Вгору неможливо.");
        }
    }

    public void down() {
        if (canDown()) {
            hoover.down();
        } else {
            JOptionPane.showMessageDialog(null, "Помилка! Вниз неможливо.");
        }
    }
}
