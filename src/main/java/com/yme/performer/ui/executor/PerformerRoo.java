package com.yme.performer.ui.executor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class PerformerRoo {


    public enum Direction {TOP, RIGHT, BOTTOM, LEFT;}
    private int x;
    private BufferedImage imgLeft;
    private BufferedImage imgRight;
    private BufferedImage imgUp;
    private BufferedImage imgDown;

    private int y;
    private Direction direction;
    private List<Trail> trails;
    public PerformerRoo() {
        x = 10;
        y = 10;
        direction = Direction.RIGHT;
        trails = new ArrayList<Trail>();
        initImages();
    }

    public void jump() {
        switch (direction) {
            case TOP:
                y--;
                break;
            case RIGHT:
                x++;
                break;
            case BOTTOM:
                y++;
                break;
            case LEFT:
                x--;
                break;
        }
    }

    public void rotate() {
        switch (direction) {
            case TOP:
                direction = Direction.LEFT;
                break;
            case RIGHT:
                direction = Direction.TOP;
                break;
            case BOTTOM:
                direction = Direction.RIGHT;
                break;
            case LEFT:
                direction = Direction.BOTTOM;
                break;
        }
    }

    public void step() {
        Trail trail = new Trail(x, y);
        trail.setDirection(direction);
        trails.add(trail);
        jump();
    }

    public void draw(Graphics g, int cellSize) {
        drawTrails(g, cellSize);
        g.setColor(Color.BLACK);
        g.setColor(Color.BLACK);
        switch (getDirection()) {
            case TOP:
                g.drawImage(imgUp, (getX() + 2) * cellSize - 81, (getY() + 2 - 1) * cellSize - 19, null);
                break;
            case RIGHT:
                g.drawImage(imgRight, (getX() + 2) * cellSize - 8, (getY() + 2) * cellSize - 58, null);
                break;
            case BOTTOM:
                g.drawImage(imgDown, (getX() + 2 - 1) * cellSize, (getY() + 2) * cellSize - 23, null);
                break;
            case LEFT:
                g.drawImage(imgLeft, (getX() + 2 - 1) * cellSize - 41, (getY() + 2 - 1) * cellSize - 28, null);
                break;
        }
    }

    private void drawTrails(Graphics g, int cellSize) {
        g.setColor(Color.BLACK);
        for (PerformerRoo.Trail t : getTrails()) {
            int fixedX = t.getX() + 2;
            int fixedY = t.getY() + 2;
            switch (t.getDirection()) {
                case TOP:
                    g.drawLine(fixedX * cellSize, fixedY * cellSize, fixedX * cellSize, (fixedY - 1) * cellSize);
                    break;
                case RIGHT:
                    g.drawLine(fixedX * cellSize, fixedY * cellSize, (fixedX + 1) * cellSize, fixedY * cellSize);
                    break;
                case BOTTOM:
                    g.drawLine(fixedX * cellSize, fixedY * cellSize, fixedX * cellSize, (fixedY + 1) * cellSize);
                    break;
                case LEFT:
                    g.drawLine(fixedX * cellSize, fixedY * cellSize, (fixedX - 1) * cellSize, fixedY * cellSize);
                    break;
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    public List<Trail> getTrails() {
        return trails;
    }

    private void initImages() {
        BufferedImage left = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("left.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/roo").toURI());
            left = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgLeft = left;

        BufferedImage right = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("right.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/roo").toURI());
            right = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgRight = right;

        BufferedImage up = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("up.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/roo").toURI());
            up = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgUp = up;

        BufferedImage down = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("down.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/roo").toURI());
            down = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgDown = down;
    }

    public class Trail {
        private int x;
        private int y;
        private Direction direction;

        public Trail(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public Direction getDirection() {
            return direction;
        }
    }
}
