package com.yme.performer.ui.executor;

import java.awt.*;
import java.awt.geom.AffineTransform;

public class PerformerTurtle
{
    public static int PERFORMER_SIZE = 10;
    private static int PERFORMER_DIRECTION_LEN = 4;

    private double x, y;
    private double angle;
    private Color color;
    private boolean drawFlag = true;

    public PerformerTurtle(double x, double y)
    {
        this.x = x;
        this.y = y;
        this.angle = Math.toRadians(180.0);
        color = new Color(0xff0000);
    }

    public void draw(Graphics g)
    {
        Graphics2D graphics2D = (Graphics2D) g.create();

        graphics2D.rotate(angle, getX(), getY());
        graphics2D.setColor(Color.BLACK);
        graphics2D.drawRect((int) getX() - PERFORMER_SIZE / 2, (int) getY() - PERFORMER_SIZE / 2, PERFORMER_SIZE, PERFORMER_SIZE);
        graphics2D.drawLine((int) getX(), (int) getY() + PERFORMER_SIZE / 2, (int) getX(), (int) getY() + PERFORMER_SIZE / 2 + PERFORMER_DIRECTION_LEN);

        graphics2D.setColor(this.color);
        graphics2D.drawLine((int) getX(), (int) getY(), (int) getX(), (int) getY());

        graphics2D.dispose();
    }

    public void forward(double pts)
    {
        AffineTransform transform = AffineTransform.getTranslateInstance(getX(), getY());
        transform.concatenate(AffineTransform.getRotateInstance(this.angle));
        transform.concatenate(AffineTransform.getTranslateInstance(0, pts));

        this.x = transform.getTranslateX();
        this.y = transform.getTranslateY();

        //Log.info("x: " + Integer.toString(x) + " y: " + Integer.toString(y));
    }

    public void back(double pts)
    {
        forward(-pts);
    }

    public void right(double pts)
    {
        this.angle += Math.toRadians(pts);
    }

    public void left(double pts)
    {
        this.angle -= Math.toRadians(pts);
    }

    public void draw()
    {
        this.drawFlag = true;
    }

    public void nodraw()
    {
        this.drawFlag = false;
    }

    public void color(int color)
    {
        this.color = new Color(color);
    }

    public void stop()
    {

    }

    // getters setters
    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getAngle()
    {
        return angle;
    }

    public Color getColor() {
        return color;
    }

    public boolean isDrawFlag() {
        return drawFlag;
    }

}
