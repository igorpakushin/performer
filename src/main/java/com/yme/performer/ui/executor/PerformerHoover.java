package com.yme.performer.ui.executor;

import com.yme.performer.ui.component.hoover.Trash;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class PerformerHoover {
    private int x;
    private int y;
    private Trash savedTrash;
    private BufferedImage imgHorizontal;
    private BufferedImage imgVertical;
    private BufferedImage imgVH;
    private BufferedImage imgHV;
    private BufferedImage imgHead;
    private BufferedImage imgHole;

    private List<Trash> bag;

    public PerformerHoover() {
        x = 0;
        y = 0;
        bag = new ArrayList<Trash>();
        initImages();
    }

    public boolean isBagEmpty() {
        return bag.isEmpty();
    }

    public List<Trash> getBag() {
        return bag;
    }

    public void setBag(List<Trash> bag) {
        this.bag = bag;
    }

    public void left() {
        x--;
    }

    public void right() {
        x++;
    }

    public void up() {
        y--;
    }

    public void down() {
        y++;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Trash getSavedTrash() {
        return savedTrash;
    }

    public void setSavedTrash(Trash savedTrash) {
        this.savedTrash = savedTrash;
    }

    private void initImages() {
        BufferedImage imgHorizontal = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("horizontal.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgHorizontal = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgHorizontal = imgHorizontal;

        BufferedImage imgVertical = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("vertical.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgVertical = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgVertical = imgVertical;

        BufferedImage imgVH = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("vh.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgVH = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgVH = imgVH;

        BufferedImage imgHV = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("hv.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgHV = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgHV = imgHV;

        BufferedImage imgHead = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("head.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgHead = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgHead = imgHead;

        BufferedImage imgHole = null;
        try {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String fileName) {
                    return ("hole.png").equals(fileName);
                }
            };

            File images = new File(ClassLoader.getSystemResource("img/hoover/hose").toURI());
            imgHole = ImageIO.read(images.listFiles(filenameFilter)[0]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.imgHole = imgHole;
    }

    public BufferedImage getImgHorizontal() {
        return imgHorizontal;
    }

    public BufferedImage getImgVertical() {
        return imgVertical;
    }

    public BufferedImage getImgVH() {
        return imgVH;
    }

    public BufferedImage getImgHV() {
        return imgHV;
    }

    public BufferedImage getImgHead() {
        return imgHead;
    }

    public BufferedImage getImgHole() {
        return imgHole;
    }
}
