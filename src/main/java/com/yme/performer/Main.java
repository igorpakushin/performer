package com.yme.performer;

import com.yme.performer.forms.hoover.MainFormViewHooverController;
import com.yme.performer.forms.roo.MainFormViewRooController;
import com.yme.performer.forms.turtle.MainFormViewTurtleController;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
//        Locale.setDefault(new Locale("ru"));
        Locale.setDefault(new Locale("uk"));
//        Locale.setDefault(Locale.US);
        new MainFormViewRooController();
        new MainFormViewHooverController();
        new MainFormViewTurtleController();
    }
}
