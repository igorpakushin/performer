package com.yme.performer;

public interface CallbackListener {
    public boolean onCallback(String method, String[] args);
}
