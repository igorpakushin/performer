package com.yme.performer.interpretator;

import com.yme.performer.interpretator.turtle.InterpretatorTurtle;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;

public class InterpretatorTurtleTest {
    private static Logger log = Logger.getLogger(InterpretatorTurtleTest.class.getName());

    @Test
    public void shouldWorkWithMovementAndDrawingCommandsLocaleDefault() {
        testMovementAndDrawingCommands();
    }

    @Test
    public void shouldWorkWithMovementAndDrawingCommandsLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testMovementAndDrawingCommands();
    }

    @Test
    public void shouldWorkWithMovementAndDrawingCommandsLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testMovementAndDrawingCommands();
    }

    @Test
    public void shouldWorkWithVariablesLocaleDefault() {
        testVariables();
    }

    @Test
    public void shouldWorkWithVariablesLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testVariables();
    }

    @Test
    public void shouldWorkWithVariablesLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testVariables();
    }

    @Test
    public void shouldLoopWithRepeatLocaleDefault() {
        testRepeatLoop();
    }

    @Test
    public void shouldLoopWithRepeatLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testRepeatLoop();
    }

    @Test
    public void shouldLoopWithRepeatLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testRepeatLoop();
    }

    @Test
    public void shouldBranchWithIfStatementLocaleDefault() {
        testIfElseStatements();
    }

    @Test
    public void shouldBranchWithIfStatementLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testIfElseStatements();
    }

    @Test
    public void shouldBranchWithIfStatementLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testIfElseStatements();
    }

    @Test
    public void shouldWorkWithProceduresLocaleDefault() {
        testProcedureSyntax();
    }

    @Test
    public void shouldWorkWithProceduresLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testProcedureSyntax();
    }

    @Test
    public void shouldWorkWithProceduresLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testProcedureSyntax();
    }

    @Test
    public void shouldCallProceduresRecursivelyLocaleDefault() {
        testRecursiveCalls();
    }

    @Test
    public void shouldCallProceduresRecursivelyLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testRecursiveCalls();
    }

    @Test
    public void shouldCallProceduresRecursivelyLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testRecursiveCalls();
    }

    @Test
    public void shouldWorkWithParenthesisLocaleDefault() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // movement commands
        String forward = bundle.getString("forward").toLowerCase();

        String rubyCode = "\n" +
                ":a1A=25 \n" +
                forward + " 5 \n" +
                forward + " :a1A " + forward + " 10 + (:a1A - 10*2) + :a1A / 5 \n" +
                forward + " (10 + :a1A - 10*2) + :a1A / 5 \n" +
                forward + " (10) + :a1A - 10*2 + :a1A / 5 \n" +
                forward + " (-10) + :a1A - 10*2 + :a1A / 5 \n" +
                forward + " (-10) + :a1A - 10*2 + (:a1A / 5) \n" +

                forward + " (-10) + (:a1A - 10*2 + (:a1A / 5)) \n" +
                "\n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(14, result.size());

        assertEquals("forward", result.get(0));
        assertEquals("5",       result.get(1));
        assertEquals("forward", result.get(2));
        assertEquals("25",      result.get(3));
        assertEquals("forward", result.get(4));
        assertEquals("20",      result.get(5));
        assertEquals("forward", result.get(6));
        assertEquals("20",      result.get(7));
        assertEquals("forward", result.get(8));
        assertEquals("20",      result.get(9));
        assertEquals("forward", result.get(10));
        assertEquals("0",       result.get(11));
        assertEquals("forward", result.get(12));
        assertEquals("0",       result.get(13));
    }

    private void testVariables() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // movement commands
        String forward = bundle.getString("forward").toLowerCase();

        String rubyCode = "\n" +
                ":a1A=25 \n" +
                forward + " 5 \n" +
                forward + " :a1A " + forward + " 10 + :a1A - 10*2 + :a1A / 5 \n" +
                "\n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(6, result.size());

        assertEquals("forward", result.get(0));
        assertEquals("5",      result.get(1));
        assertEquals("forward", result.get(2));
        assertEquals("25",      result.get(3));
        assertEquals("forward", result.get(4));
        assertEquals("20",      result.get(5));
    }

    private void testMovementAndDrawingCommands() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // movement commands
        String forward = bundle.getString("forward").toLowerCase();
        String back = bundle.getString("back").toLowerCase();
        String right = bundle.getString("right").toLowerCase();
        String left = bundle.getString("left").toLowerCase();

        // drawing commands
        String draw = bundle.getString("draw").toLowerCase();
        String nodraw = bundle.getString("nodraw").toLowerCase();
        String color = bundle.getString("color").toLowerCase();
        String cls = bundle.getString("cls").toLowerCase();
        String stop = bundle.getString("stop").toLowerCase();

        String rubyCode = "\n" +
                forward + " 51 \n" +
                back + " 5 \n" +
                right + " 5 \n" +
                left + " 5 \n" +

                draw + "\n" +
                nodraw + "\n" +
                color + " 5 \n" +
                cls + "\n" +
                stop + "\n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(14, result.size());

        assertEquals("forward", result.get(0));
        assertEquals("51", result.get(1));
        assertEquals("back", result.get(2));
        assertEquals("5", result.get(3));
        assertEquals("right", result.get(4));
        assertEquals("5", result.get(5));
        assertEquals("left", result.get(6));
        assertEquals("5", result.get(7));
        assertEquals("draw", result.get(8));
        assertEquals("nodraw", result.get(9));
        assertEquals("color", result.get(10));
        assertEquals("5", result.get(11));
        assertEquals("cls", result.get(12));
        assertEquals("stop", result.get(13));
    }

    private void testRepeatLoop() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // whatever command for test
        String forward = bundle.getString("forward").toLowerCase();

        String sourceCode = "\n" +
                ":n = 3" + "\n" +
                ":a = 5" + "\n" +
                "repeat 2 [" + "\n" +
                "    repeat :n [ " + forward + " :a ]" + "\n" +
                "]";

        InterpretResult interpretResult = new InterpretResult(sourceCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(12, result.size());

        assertEquals("forward", result.get(0));
        assertEquals("5", result.get(1));
        assertEquals("forward", result.get(2));
        assertEquals("5", result.get(3));
        assertEquals("forward", result.get(4));
        assertEquals("5", result.get(5));
        assertEquals("forward", result.get(6));
        assertEquals("5", result.get(7));
        assertEquals("forward", result.get(8));
        assertEquals("5", result.get(9));
        assertEquals("forward", result.get(10));
        assertEquals("5", result.get(11));
    }

    private String constructTestWithOperator(String operator, String command) {
        return
                ":a = 3" + "\n" +
                ":b = 5" + "\n" +
                "if :a + 3 " + operator + " :b [" + "\n" +
                "    :a = :a + 1" + "\n" +
                "    :b = :b + 1" + "" +
                "]  [ \n" +
                "    :a = :a - 1" + "\n" +
                "    :b = :b - 1" + "\n" +
                "]" + "\n" +
                command + " :a \n" +
                command + " :b \n";
    }

    private void testIfElseStatements() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // whatever command for test
        String forward = bundle.getString("forward").toLowerCase();

        String sourceCode = "\n" +
                // test > (greater)
                constructTestWithOperator(">", forward) +

                // test >= (greater or equal)
                constructTestWithOperator(">=", forward) +

                // test = (equal)
                constructTestWithOperator("=", forward) +

                // test <> (not equal)
                constructTestWithOperator("<>", forward) +

                // test < (less)
                constructTestWithOperator("<", forward) +

                // test <= (less or equal)
                constructTestWithOperator("<=", forward)
        ;

        InterpretResult interpretResult = new InterpretResult(sourceCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(24, result.size());

        // >
        assertEquals("forward", result.get(0));
        assertEquals("4", result.get(1));
        assertEquals("forward", result.get(2));
        assertEquals("6", result.get(3));

        // >=
        assertEquals("forward", result.get(4));
        assertEquals("4", result.get(5));
        assertEquals("forward", result.get(6));
        assertEquals("6", result.get(7));

        // =
        assertEquals("forward", result.get(8));
        assertEquals("2", result.get(9));
        assertEquals("forward", result.get(10));
        assertEquals("4", result.get(11));

        // <>
        assertEquals("forward", result.get(12));
        assertEquals("4", result.get(13));
        assertEquals("forward", result.get(14));
        assertEquals("6", result.get(15));

        // <
        assertEquals("forward", result.get(16));
        assertEquals("2", result.get(17));
        assertEquals("forward", result.get(18));
        assertEquals("4", result.get(19));

        // <=
        assertEquals("forward", result.get(20));
        assertEquals("2", result.get(21));
        assertEquals("forward", result.get(22));
        assertEquals("4", result.get(23));
    }

    private void testProcedureSyntax() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // whatever command for test
        String forward = bundle.getString("forward").toLowerCase();

        String sourceCode = "\n"+
                "to no_param_function" + "\n" +
                "    :a = 5" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to one_param_function :arg1" + "\n" +
                "    :a = 5 + :arg1" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to two_param_function :ar_g1 :arg2" + "\n" +
                "    :a = 5 + :ar_g1 + :arg2" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to three_param_function :arg1 :arg2 :arg3" + "\n" +
                "    :a = 5 + :arg1 + :arg2 + :arg3" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to four_param_function :arg1 :arg2 :arg3 :arg4" + "\n" +
                "    :a = 5 + :arg1 + :arg2 + :arg3 + :arg4" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to five_param_function :arg1 :arg2 :arg3 :arg4 :arg5" + "\n" +
                "    :a = 5 + :arg1 + :arg2 + :arg3 + :arg4 + :arg5" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                "to twenty_param_function :arg1 :arg2 :arg3 :arg4 :arg5 :arg6 :arg7 :arg8 :arg9 :arg10 :arg11 :arg12 :arg13 :arg14 :arg15 :arg16 :arg17 :arg18 :arg19 :arg20" + "\n" +
                "    :a = 5 + :arg1 + :arg2 + :arg3 + :arg4 + :arg5" + "\n" +
                "    :a = :a + :arg6 + :arg7 + :arg8 + :arg9 + :arg10" + "\n" +
                "    :a = :a + :arg11 + :arg12 + :arg13 + :arg14 + :arg15" + "\n" +
                "    :a = :a + :arg16 + :arg17 + :arg18 + :arg19 + :arg20" + "\n" +
                "    " + forward + " :a\n" +
                "end\n" +

                ":arg1 = 1" + "\n" +
                ":arg2 = 2" + "\n" +
                ":arg3 = 3" + "\n" +
                ":arg4 = 4" + "\n" +
                ":arg5 = 5" + "\n" +

                ":arg6 = 6" + "\n" +
                ":arg7 = 7" + "\n" +
                ":arg8 = 8" + "\n" +
                ":arg9 = 9" + "\n" +
                ":arg10 = 10" + "\n" +
                ":arg11 = 11" + "\n" +
                ":arg12 = 12" + "\n" +
                ":arg13 = 13" + "\n" +
                ":arg14 = 14" + "\n" +
                ":arg15 = 15" + "\n" +
                ":arg16 = 16" + "\n" +
                ":arg17 = 17" + "\n" +
                ":arg18 = 18" + "\n" +
                ":arg19 = 19" + "\n" +
                ":arg20 = 20" + "\n" +

                "no_param_function\n" +
                "one_param_function    :arg1\n" +
                "two_param_function    :arg1 :arg2\n" +
                "three_param_function  :arg1 :arg2 :arg3\n" +
                "four_param_function   :arg1 :arg2 :arg3 :arg4\n" +
                "five_param_function   :arg1 :arg2 :arg3 :arg4 :arg5\n" +
                "twenty_param_function :arg1 :arg2 :arg3 :arg4 :arg5 :arg6 :arg7 :arg8 :arg9 :arg10 :arg11 :arg12 :arg13 :arg14 :arg15 :arg16 :arg17 :arg18 :arg19 :arg20\n" +
                "\n";

        InterpretResult interpretResult = new InterpretResult(sourceCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(14, result.size());

        // no param
        assertEquals("forward", result.get(0));
        assertEquals("5", result.get(1));

        // 1 param
        assertEquals("forward", result.get(2));
        assertEquals("6", result.get(3));

        // 2 param
        assertEquals("forward", result.get(4));
        assertEquals("8", result.get(5));

        // 3 param
        assertEquals("forward", result.get(6));
        assertEquals("11", result.get(7));

        // 4 param
        assertEquals("forward", result.get(8));
        assertEquals("15", result.get(9));

        // 5 param
        assertEquals("forward", result.get(10));
        assertEquals("20", result.get(11));

        // 20 param
        assertEquals("forward", result.get(12));
        assertEquals("215", result.get(13));
    }

    private void testRecursiveCalls() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewTurtle");

        // whatever command for test
        String forward = bundle.getString("forward").toLowerCase();

        String sourceCode = "\n"+
                ":counter = 0" + "\n" +

                "to recursive_sum :arg" + "\n" +
                "    if :arg > 0 [ "  + "\n" +
                "        return :arg + (recursive_sum :arg-1) " + "\n" +
                "    ]" + "\n" +
                "    return 0" + "\n" +
                "end" + "\n" +

                ":result = recursive_sum 5" + "\n" +
                forward + " :result" + "\n" +
                ":result = recursive_sum 200" + "\n" +
                forward + " :result" + "\n" +
                "";

        InterpretResult interpretResult = new InterpretResult(sourceCode).invoke(null, new InterpretatorTurtle());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(4, result.size());

        assertEquals("forward", result.get(0));
        assertEquals("15", result.get(1));

        assertEquals("forward", result.get(2));
        assertEquals("20100", result.get(3));
    }
}
