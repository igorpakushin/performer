package com.yme.performer.interpretator;

import com.yme.performer.CallbackListener;
import com.yme.performer.interpretator.roo.InterpretatorRoo;

import java.util.ArrayList;
import java.util.List;

public class InterpretResult {
    private String rubyCode;
    private List<String> result;
    private Interpretator.ErrorCode e;

    public InterpretResult(String rubyCode) {
        this.rubyCode = rubyCode;
    }

    public List<String> getResult() {
        return result;
    }

    public Interpretator.ErrorCode getE() {
        return e;
    }

    public InterpretResult invoke(final CustomCallback callback, InterpretatorImpl instance) {
        result = new ArrayList<String>();
        e = instance.execute(new CallbackListener() {
            @Override
            public boolean onCallback(String str, String[] args) {
                result.add(str);
                for (String arg : args) result.add(arg);
                if (callback != null) return callback.performCustomCallback(str, args);
                return false;
            }
        }, rubyCode);
        return this;
    }

    public interface CustomCallback {
        public boolean performCustomCallback(String str, String[] args);
    }
}
