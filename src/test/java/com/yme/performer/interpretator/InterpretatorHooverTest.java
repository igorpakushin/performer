package com.yme.performer.interpretator;

import com.yme.performer.interpretator.hoover.InterpretatorHoover;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;

public class InterpretatorHooverTest {
    private static Logger log = Logger.getLogger(InterpretatorHooverTest.class.getName());

    @Test
    public void shouldWorkWithBasicCommandsLocaleDefault() {
        testBasicCommands();
    }

    @Test
    public void shouldWorkWithBasicCommandsLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testBasicCommands();
    }

    @Test
    public void shouldWorkWithBasicCommandsLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testBasicCommands();
    }

    @Test
    public void showWorkWithConditionalCommandsLocaleDefault() {
        testConditionalCommands();
    }

    @Test
    public void showWorkWithConditionalCommandsLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testConditionalCommands();
    }

    @Test
    public void showWorkWithConditionalCommandsLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testConditionalCommands();
    }

    @Test
    public void shouldWorkWithBranchingOperatorMethodAndLoopLocaleDefault() {
        testBranchingOperatorMethodAndLoop();
    }

    @Test
    public void shouldWorkWithBranchingOperatorMethodAndLoopLocaleRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        testBranchingOperatorMethodAndLoop();
    }

    @Test
    public void shouldWorkWithBranchingOperatorMethodAndLoopLocaleUK() {
        Locale.setDefault(new Locale("uk", "UA"));
        testBranchingOperatorMethodAndLoop();
    }

    private void testBranchingOperatorMethodAndLoop() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

        String procedure = bundle.getString("procedure").toLowerCase();
        String operator_if = bundle.getString("if").toLowerCase();
        String operator_while = bundle.getString("while").toLowerCase();
        String end = bundle.getString("end").toLowerCase();

        String left = bundle.getString("left").toLowerCase();

        String rubyCode =
                procedure + "testProc\n" +
                    operator_if + "true\n" +
                        "i = 5 \n" +
                        operator_while + "i > 0 \n" +
                            "i -= 1 \n" +
                            left + "\n" +
                        end + "\n" +
                    end + "\n" +
                end + "\n" +
                "testProc";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorHoover());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(5, result.size());
        assertEquals("left", result.get(0));
        assertEquals("left", result.get(1));
        assertEquals("left", result.get(2));
        assertEquals("left", result.get(3));
        assertEquals("left", result.get(4));
    }

    private void testConditionalCommands() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

        String can_left = bundle.getString("can_left").toLowerCase();
        String can_right = bundle.getString("can_right").toLowerCase();
        String can_up = bundle.getString("can_up").toLowerCase();
        String can_down = bundle.getString("can_down").toLowerCase();

        String is_bag_empty = bundle.getString("is_bag_empty").toLowerCase();
        String is_bag_not_empty = bundle.getString("is_bag_not_empty").toLowerCase();
        String is_cell_empty = bundle.getString("is_cell_empty").toLowerCase();
        String is_cell_not_empty = bundle.getString("is_cell_not_empty").toLowerCase();

        String rubyCode =
                "if " + can_left + "\n" +
                "    if " + can_right + "\n" +
                "        if " + can_up + "\n" +
                "            if " + can_down + "\n" +
                "                if " + is_bag_empty + "\n" +
                "                    if " + is_bag_not_empty + "\n" +
                "                        if " + is_cell_empty + "\n" +
                "                            if " + is_cell_not_empty + "\n" +
                "                            end \n" +
                "                        end \n" +
                "                    end \n" +
                "                end \n" +
                "            end \n" +
                "        end \n" +
                "    end \n" +
                "end \n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(new InterpretResult.CustomCallback() {
            @Override
            public boolean performCustomCallback(String str, String[] args) {
                return true;
            }
        }, new InterpretatorHoover());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(8, result.size());
        assertEquals("can_left", result.get(0));
        assertEquals("can_right", result.get(1));
        assertEquals("can_up", result.get(2));
        assertEquals("can_down", result.get(3));

        assertEquals("is_bag_empty", result.get(4));
        assertEquals("is_bag_not_empty", result.get(5));
        assertEquals("is_cell_empty", result.get(6));
        assertEquals("is_cell_not_empty", result.get(7));
    }

    private void testBasicCommands() {
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewHoover");

        String left = bundle.getString("left").toLowerCase();
        String right = bundle.getString("right").toLowerCase();
        String up = bundle.getString("up").toLowerCase();
        String down = bundle.getString("down").toLowerCase();

        String take = bundle.getString("take").toLowerCase();
        String put = bundle.getString("put").toLowerCase();
        String save = bundle.getString("save").toLowerCase();


        String rubyCode =
                left + "\n" +
                right + "\n" +
                up + "\n" +
                down + "\n" +
                take + "\n" +
                put + "\n" +
                save;

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorHoover());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(7, result.size());
        assertEquals("left", result.get(0));
        assertEquals("right", result.get(1));
        assertEquals("up", result.get(2));
        assertEquals("down", result.get(3));

        assertEquals("take", result.get(4));
        assertEquals("put", result.get(5));
        assertEquals("save", result.get(6));
    }
}
