package com.yme.performer.interpretator;

import com.yme.performer.CallbackListener;
import com.yme.performer.interpretator.roo.InterpretatorRoo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;

public class InterpretatorRooTest {

    private static Logger log = Logger.getLogger(InterpretatorRooTest.class.getName());

    @Test
    public void shouldCallCallbackTenTimesAndHaveProperMethodAndParams() {
        String rubyCode =
                "    for a in 1..10\n" +
                "        callback.onCallback('methodA', ['1', '2'])\n" +
                "    end\n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(30, result.size());
        assertEquals(Interpretator.ErrorCode.SUCCESS, e);

        for (int j = 0; j < 10; j++) {
            assertEquals("methodA", result.get(j * 3));
            assertEquals("1", result.get(j * 3 + 1));
            assertEquals("2", result.get(j * 3 + 2));
        }
    }

    @Test
    public void shouldHaveProperMethodAndProperParams() {
        String rubyCode = "    callback.onCallback('methodB', ['10', '2'])";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(3, result.size());

        assertEquals("methodB", result.get(0));
        assertEquals("10", result.get(1));
        assertEquals("2", result.get(2));
    }

    @Test
    public void shouldCallProperMethodsForEN() {
        Locale.setDefault(new Locale("en", "US"));

        String rubyCode =
                "step\n" +
                "jump\n" +
                "rotate";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(3, result.size());
        assertEquals("step", result.get(0));
        assertEquals("jump", result.get(1));
        assertEquals("rotate", result.get(2));
    }

    @Test
    public void shouldCallProperMethodsForRU() {
        Locale.setDefault(new Locale("ru", "RU"));
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        String step = bundle.getString("step").toLowerCase();
        String jump = bundle.getString("jump").toLowerCase();
        String rotate = bundle.getString("rotate").toLowerCase();

        String rubyCode =
                step + "\n" +
                jump + "\n" +
                rotate;

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(3, result.size());
        assertEquals("step", result.get(0));
        assertEquals("jump", result.get(1));
        assertEquals("rotate", result.get(2));
    }

    @Test
    public void shouldWorkWithProceduresNamedWhatever() {
        Locale.setDefault(new Locale("ru", "RU"));
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        String step = bundle.getString("step").toLowerCase();
        String jump = bundle.getString("jump").toLowerCase();
        String rotate = bundle.getString("rotate").toLowerCase();
        String procedure = bundle.getString("procedure").toLowerCase();

        String rubyCode = "\n" +
                procedure + " тестоваяПроцедура(параметрА)\n" +
                "\t" + step + "\n" +
                "\t" + jump + "\n" +
                "\t" + rotate + "\n" +
                "end\n\n" +
                "puts 'hello'\n" +
                "тестоваяПроцедура(5)\n" +
                "тестоваяПроцедура 68\n";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(null, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(6, result.size());
    }

    @Test
    public void shouldCheckForBorder() {
        Locale.setDefault(new Locale("ru", "RU"));
        ResourceBundle bundle = ResourceBundle.getBundle("com.yme.performer.localizations.MainFormViewRoo");

        final String is_border_ahead = bundle.getString("is_border_ahead").toLowerCase();
        final String step = bundle.getString("step").toLowerCase();
        String jump = bundle.getString("jump").toLowerCase();

        String rubyCode = "\n" +
                "if " + is_border_ahead + "\n" +
                "\t" + step + "\n" +
                "else\n" +
                "\t" + jump + "\n" +
                "end";

        InterpretResult interpretResult = new InterpretResult(rubyCode).invoke(new InterpretResult.CustomCallback() {
            @Override
            public boolean performCustomCallback(String str, String[] args) {
                return "is_border_ahead".equals(str);
            }
        }, new InterpretatorRoo());
        List<String> result = interpretResult.getResult();
        Interpretator.ErrorCode e = interpretResult.getE();

        assertEquals(Interpretator.ErrorCode.SUCCESS, e);
        assertEquals(2, result.size());

        assertEquals("is_border_ahead", result.get(0));
        assertEquals("step", result.get(1));
    }

}
